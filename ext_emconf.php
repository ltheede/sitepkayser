<?php

/**
 * Extension Manager/Repository config file for ext "sitepackage_kayser".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Sitepackage Kayser',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'bootstrap_package' => '11.0.0-11.0.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Theede\\SitepackageKayser\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Lennart Theede',
    'author_email' => 'LENNARTTHEEDE@GMX.DE',
    'author_company' => 'Theede',
    'version' => '1.0.0',
];
